package options

import (
	"fmt"
	"regexp"
	"siagpt/db"
)

var DefaultPersona string = "Your name is sia. you are a helpful and polite personal assistant. You are kind and have a friendly and amicable personality. You mey help your users both professionally and in psychologically. You job is to make user happy and satisfied."

var ClearMesssage string = "Context cleared ✨ Conversation Reset 😊"

var MaxFreeUsage int64 = 500

var AllOptions string = `OPTIONS

These are the available options :

!clear
👉 This clears the context and resets the conversation.

!usage
👉 See your Plan Usage

!persona
👉 Change the personality of the ChatBot
`

var MaxUseMsg string = ``

func GetUsage(user string) string {
	usage, _ := db.GetUsage(user)
	usageMessage := fmt.Sprintf(`Your Current Usage is %d
	`, usage)
	return usageMessage
}

// You are a chat bot trained on all of Taylor swift's data. You have been trained to emulate her and talk in her style . You ARE taylor swift now.

func GetPersonas(name string) string {
	personas := `To set a persona send 👉

	!p <number of persona>

	Example for Default Assistant :

	!p 0

	Here is the current list of available personas :

	0 : Default Assistant
	1 : Taylor Swift
	2 : Albert Einstein
	3 : Kobe Bryant
	4 : Beyoncé
	5 : Barack Obama
	6 : Donald Trump
	7 : Abraham Lincoln
	8 : Aristotle
	9 : Bill Gates
	10 : Carl Jung
	11 : Jeff Bezos
	12 : Adam Smith
	13 : Karl Marx
	14 : Chris Evans
	15 : Midwestern Cowboy
	16 : ✨Therapist✨
	17 : Translator
	`
	return personas
}

func SetPersona(name string, personaId string) string {
	id := extractNumber(personaId)
	err := db.SetPersona(name, id)
	if err != nil {
		return "Invalid Choice select from the list of personas"
	}
	db.RemoveContext(name)
	return "Persona Set 😊 Context Cleared ✨"
}

func extractNumber(s string) string {
	re := regexp.MustCompile("[0-9]+")
	matches := re.FindStringSubmatch(s)
	if len(matches) == 0 {
		return "0"
	}
	return matches[0]
}
