// package main

// import (
// 	"siagpt/db"
// 	"fmt"
// )

// func main() {
// 	db.Connect()
// 	// // Create user
// 	err := db.CreateUser("9878491259")
// 	if err != nil {
// 		fmt.Println("Error creating user:", err)
// 		return
// 	}
// 	fmt.Println("User created successfully")

// 	// Add message to user's queue
// 	err = db.AddMessage("9878491259", "LATEST")
// 	if err != nil {
// 		fmt.Println("Error adding message:", err)
// 		return
// 	}
// 	fmt.Println("Message added successfully")

// 	// // Get user's usage
// 	// usage, err := db.GetUsage("9878491259")
// 	// if err != nil {
// 	// 	fmt.Println("Error getting usage:", err)
// 	// 	return
// 	// }
// 	// fmt.Println("User's usage:", usage)

// 	// // Get last 10 messages
// 	messages, err := db.GetLastMessages("9878491259")
// 	if err != nil {
// 		fmt.Println("Error getting messages:", err)
// 		return
// 	}
// 	fmt.Println("User's messages:", messages)
// }
