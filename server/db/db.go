package db

import (
	"context"
	"fmt"

	"github.com/redis/go-redis/v9"
)

var rdb *redis.Client

func Connect() {
	// Connect to Redis database
	rdb = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       9,  // use default database
	})

	// Test connection
	pong, err := rdb.Ping(context.Background()).Result()
	if err != nil {
		fmt.Println("Error connecting to Redis:", err)
		return
	}
	fmt.Println("Connected to Redis:", pong)
}

func CreateUser(name string) error {
	// Create hash set for user
	err := rdb.HSet(context.Background(), name, "usage", 0, "messages", "[]", "plan", 0, "persona", "0").Err()
	if err != nil {
		return err
	}

	// Set max length for messages queue
	err = rdb.LTrim(context.Background(), name+":messages", 0, 9).Err()
	if err != nil {
		return err
	}

	return nil
}

func UserExists(name string) (bool, error) {
	// Check if user exists
	exists, err := rdb.Exists(context.Background(), name).Result()
	if err != nil {
		return false, err
	}

	return exists == 1, nil
}

func AddMessage(name string, message string) error {
	// Add message to user's queue
	err := rdb.RPush(context.Background(), name+":messages", message).Err()
	if err != nil {
		return err
	}

	// Increment usage
	err = rdb.HIncrBy(context.Background(), name, "usage", 1).Err()
	if err != nil {
		return err
	}

	return nil
}

func GetUsage(name string) (int64, error) {
	// Get user's usage
	usage, err := rdb.HGet(context.Background(), name, "usage").Int64()
	if err != nil {
		return 0, err
	}

	return usage, nil
}

func GetPlan(name string) (int64, error) {
	// Get user's usage
	usage, err := rdb.HGet(context.Background(), name, "plan").Int64()
	if err != nil {
		return 0, err
	}

	return usage, nil
}

func GetLastMessages(name string) ([]string, error) {
	// Get last 10 messages
	messages, err := rdb.LRange(context.Background(), name+":messages", 0, 20).Result()
	if err != nil {
		return nil, err
	}

	return messages, nil
}

func RemoveContext(name string) error {
	// Use the LLEN command to get the length of the list
	length, err := rdb.LLen(context.Background(), name+":messages").Result()
	if err != nil {
		return err
	}

	// Use the LTRIM command to remove all elements from the list
	err = rdb.LTrim(context.Background(), name+":messages", length, 0).Err()
	if err != nil {
		return err
	}

	return nil
}

func SetPersona(name string, personaId string) error {

	_, err := rdb.HGet(context.Background(), "persona", personaId).Result()
	if err != nil {
		return err
	}

	// Set the persona field in the user's hash set
	err = rdb.HSet(context.Background(), name, "persona", personaId).Err()
	if err != nil {
		return err
	}

	return nil
}

func GetPersona(name string) string {
	// Get the persona field from the user's hash set
	personaId, _ := rdb.HGet(context.Background(), name, "persona").Result()

	personaPrompt, err := rdb.HGet(context.Background(), "persona", personaId).Result()

	if err != nil {
		defaultPersona, _ := rdb.HGet(context.Background(), "persona", "0").Result()
		return defaultPersona
	}

	return personaPrompt
}
