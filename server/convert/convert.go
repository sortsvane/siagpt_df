package convert

import (
	"os/exec"
)

func ConvertOGAtoMP3(inputFile string, outputFile string) error {
	cmd := exec.Command("ffmpeg", "-i", inputFile, "-vn", "-ar", "44100", "-ac", "1", "-ab", "64k", "-f", "mp3", outputFile)
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}
