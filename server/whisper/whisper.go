package whisper

import (
	"context"
	"fmt"

	openai "github.com/sashabaranov/go-openai"
)

func GetTranscript(filePath string) string {
	c := openai.NewClient("sk-OWzYG2vanggZ4YVMWK4hT3BlbkKJ0pNTdCkwTKGiv6GuB7oG")
	ctx := context.Background()

	req := openai.AudioRequest{
		Model:    openai.Whisper1,
		FilePath: filePath,
	}
	resp, err := c.CreateTranscription(ctx, req)
	if err != nil {
		fmt.Printf("Transcription error: %v\n", err)
		return "Transcription error"
	}
	return resp.Text
}
