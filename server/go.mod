module siagpt

go 1.19

require (
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/mdp/qrterminal/v3 v3.0.0
	github.com/sashabaranov/go-openai v1.5.6
	go.mau.fi/whatsmeow v0.0.0-20230316102651-f0c131192076
	google.golang.org/protobuf v1.30.0
)

require (
	filippo.io/edwards25519 v1.0.0 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/redis/go-redis/v9 v9.0.2 // indirect
	go.mau.fi/libsignal v0.1.0 // indirect
	golang.org/x/crypto v0.5.0 // indirect
	rsc.io/qr v0.2.0 // indirect
)
