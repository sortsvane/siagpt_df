package gpt

import (
	"context"
	"fmt"
	"log"
	"strings"

	openai "github.com/sashabaranov/go-openai"
)

var client = openai.NewClient("sk-OWzYG2vanggZ4YVMWK4hT3BlbkKJ0pNTdCkwTKGiv6GuB7oG")

func GetResponse(input []string, system string) string {
	var messages []openai.ChatCompletionMessage
	messages = append(messages, openai.ChatCompletionMessage{
		Role:    openai.ChatMessageRoleSystem,
		Content: system,
	})
	for _, in := range input {
		if strings.HasPrefix(in, "~") {
			messages = append(messages, openai.ChatCompletionMessage{
				Role:    openai.ChatMessageRoleAssistant,
				Content: strings.TrimPrefix(in, "~"),
			})
		} else {
			messages = append(messages, openai.ChatCompletionMessage{
				Role:    openai.ChatMessageRoleUser,
				Content: in,
			})
		}
	}
	log.Println(messages)
	resp, err := client.CreateChatCompletion(
		context.Background(),
		openai.ChatCompletionRequest{
			Model:    openai.GPT3Dot5Turbo,
			Messages: messages,
		},
	)

	if err != nil {
		fmt.Printf("ChatCompletion error: %v\n", err)
		return "ChatCompletion error"
	}

	return resp.Choices[0].Message.Content
}
